package com.ha.automation.dboperations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class java1 {
	private static final Logger logger = LogManager.getLogger();

	private Connection conn;

	public java1(String conString, String driver) {
		try {
			Class.forName(driver);
			this.conn = DriverManager.getConnection(conString);
		} catch (SQLException | ClassNotFoundException e) {
			logger.error(
					"Failed to get the connection to the database with the connection string: {} and the exception is: {}",
					conString, e);
			Assert.assertFalse(true, "Connection to the database failed");
		}
	}

	public void exeUpdate(String queryString) {
			Statement statement = null;
		try {
			statement = conn.createStatement();
			statement.executeUpdate(queryString);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}

}
